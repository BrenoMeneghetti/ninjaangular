import { Component } from '@angular/core';
import { RaceService } from './services/race.service';

@Component({
    selector: 'ponyracer-app',
    template: 
    `
    <h1>Pony RACER HELL {{numberOfUser}} </h1>
    <h1>{{friend?.name}}</h1>
    <ns-greeting ></ns-greeting>

    <input type="text" #name>
    <button (click)="name.focus()">Focus the input</button>

    <ns-races (newRaces)="onNewRace()" ></ns-races>
    

    <button (click)="onNewRace()">CLick me </button>
    LIST:
    <p>{{list() | json}}</p>
    `
})
export class PonyRacerAppComponent {
    numberOfUser: number = 145;
    friend: any = {name: 'JOE', name2: 'Bnda'};

    constructor(private raceService: RaceService){}

    onNewRace() {
        console.log("We are goood.");
    }

    list() {
        return this.raceService.list();
    }
}