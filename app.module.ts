import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PonyRacerAppComponent } from './app.component';
import { RacesComponent } from './races.component';
import { GreetingComponent } from './greeting.component';
import { ApiService } from './services/api.service';
import { RaceService } from './services/race.service';
import { FakeRaceService } from './services/fake.service';

const IS_PROD = false;

@NgModule({
    imports: [BrowserModule],
    declarations: [PonyRacerAppComponent, RacesComponent,GreetingComponent],
    providers: [
        ApiService, 
        { provide: 'IS_PROD', useValue: true },
        {provide: RaceService, 
            useFactory:(IS_PROD, apiService) => IS_PROD ? new RaceService(apiService) : new FakeRaceService(),
            deps: ['IS_PROD',ApiService]
        },
        { provide: 'RaceServiceToken', useClass: RaceService }
        ],
    bootstrap: [PonyRacerAppComponent]
})

export class AppModule {

}