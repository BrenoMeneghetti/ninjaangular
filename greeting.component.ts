import { Component } from '@angular/core';

@Component({
    selector: 'ns-greeting',
    template: 
    `<h2>{{asyncGreeting | async}}</h2>
    `
})

export class  GreetingComponent {

     asyncGreeting: Promise<string> = new Promise(resolve => {
        // after 1 second, the promise will resolve
        window.setTimeout(() => resolve('hello'), 1000);
      });
    

    onSpacePress() {
        console.log("Space combat.");
    }

}