import { Component } from '@angular/core';
import { RaceService } from './services/race.service';
import { FakeRaceService } from './services/fake.service';

@Component({
    selector: 'ns-races',
    providers: [{provide: RaceService, useClass: FakeRaceService}],
    template: 
    `<h2>{{atributos.nome}}</h2>
    <img src={{atributos.img}} />
    <h2>{{name}}</h2>
    <textarea (keydown.space)="onSpacePress()">Press space!</textarea>

    <div *ngIf="races.length > 0"><h2>Races</h2>
    <ul>
    <li *ngFor="let race of races">{{race.name}}</li>
    </ul>
    </div>
    <button (click)="newRaces()">New Races</button>
    `
})

export class  RacesComponent {
    atributos: any = {nome: 'WEEE', img: 'http://lorempixel.com/output/people-q-c-640-480-9.jpg'};
    name: any = 'T2';
    contador: number = 1;
    races: Array<any> = [];
    constructor (private raceService: RaceService) {

    }
    
    list() {
        return this.raceService.list();
    }

    newRaces() {
        this.races = this.list();
        this.atributos.nome= "Ativou no clique" + this.contador++;
    }
    onSpacePress() {
        console.log("Space combat.");
    }

}