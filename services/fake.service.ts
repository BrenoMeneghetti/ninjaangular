export class FakeRaceService {
    list() {
        return [ 
            { name: 'Rainbow Dash' },
            { name: 'Pinkie Pie' },
            { name: 'Fluttershy' },
            { name: 'Rainbow Dash2' },
            { name: 'Pinkie Pie2' },
            { name: 'Fluttershy2' }
        ];
    }
}